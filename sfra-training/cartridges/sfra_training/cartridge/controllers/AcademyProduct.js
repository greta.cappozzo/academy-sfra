'use sctrict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var URLUtils = require('dw/web/URLUtils');
var productMgr = require('dw/catalog/ProductMgr');
var Site = require('dw/system/Site');
var HookMgr = require('dw/system/HookMgr');
var Logger = require('dw/system/Logger');


server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {
        var isAcademyProductActive = Site.getCurrent().getCustomPreferenceValue('isAcademyProductActive');
        var prodID = Site.getCurrent().getCustomPreferenceValue('testpref');
        var customlogger = Logger.getLogger('loggerTest', 'warn');
        customlogger.warn('ciao!')

        if (isAcademyProductActive == true) {
            var productID = "008884303989";
            var product = productMgr.getProduct(productID);

            var datiprod = {}
            datiprod = dw.system.HookMgr.callHook('app.testhook', 'dati', product);

            res.render('home/academyproduct/academyproduct', {
                productID: product
            });
            res.render('home/academyproduct/academyproduct', datiprod) 
       
             /*var product = productMgr.getProduct(prodID);

              var CustomObject = dw.system.HookMgr.callHook('app.objhook', 'objInfo', product);

              res.render('home/academyproduct/academyproduct', {
                  prodID : product.ID,
                  prodName : product.name,
                  prodDesc : product.shortDescription,
                  custom : CustomObject
              }) */


        } else {

            res.render('home/academyproduct/academyproducterror');
        }




        next();

    });

server.get(
    'Preference',
    server.middleware.https,
    function (req, res, next) {
        // var isProductActive = Site.getCurrent().getCustomPreferenceValue('testpref');

        if (Site.getCurrent().getCustomPreferenceValue('testpref')) {
            var id = req.querystring.id;
            var product = productMgr.getProduct(id);

            var datiprod = {}
            datiprod = dw.system.HookMgr.callHook('testhook', 'dati', product);

            res.render('home/academyproduct/academyproduct', datiprod);


        } else {

            res.render('home/academyproduct/academyproducterror');
        }
        next();

    });

server.get(
    'CustObj',
    server.middleware.https,
    function (req, res, next) {


        next();

    });


module.exports = server.exports();