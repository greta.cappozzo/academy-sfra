'use strict';

/**
 * Fetches the local service registry assigned to a service id
 * @param {String} serviceId 
 * @returns 
 */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

function getService(serviceId) {

    /*  return LocalServiceRegistry.createService(serviceId, {
        createRequest: function(svc: HTTPSErvices, args) {
            // Default request method is post
            // No need to setRequestMethod
            if (args) {
                svc.addHeader("Content-Type", "text/json");
                return JSON.stringify(args);
            } else {
                return null;
            }
        },
        parseResponse: function(svc: HTTPSErvices, client) {
            return client.text;
        },*/

    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function (svc: HTTPServices, args) {
            if (args) {

                svc.addParam('hapikey', dw.system.Site.getCurrent().getCustomPreferenceValue("hapikey_preference"));
                //svc.URL = svc.URL + '?hapikey=' + dw.system.Site.getCurrent().getCustomPreferenceValue("hapikey_preference");
                // svc.addHeader("Content-Type", "text/json");
                svc.addHeader("Content-Type", "application/json");


                return JSON.stringify(args);

            } else {
                return null;
            }

        },
        parseResponse: function (svc: HTTPSErvices, client) {
            return client.text;
        },

        mockCall: function (svc: HTTPSErvices, client) {

            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        }

    });
}



//nuova funzione per lista

function getServiceGetList(serviceId) {
    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function (svc, args) {
            svc.setRequestMethod("GET");
            svc.addParam('hapikey', dw.system.Site.getCurrent().getCustomPreferenceValue("hapikey_preference"));
            svc.addParam('limit', '10');
            svc.addHeader("Content-Type", "application/json");
        },
        parseResponse: function (svc, client) {
            return client.text;
        },
        mockCall: function (svc, client) {

            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        }

    });

}



module.exports = {
    getServiceGetList: getServiceGetList,
    getService: getService
};