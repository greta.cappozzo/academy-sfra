'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');


module.exports.render = function (context, modelIn) {
    var model = modelIn || new HashMap();
    var content = context.content;

    model.videoyt = context.content.videoyt;
    model.videovimeo = context.content.videovimeo;

    return new Template('experience/components/commerce_assets/vimeoyt').render(model).text;
};