'use strict';

var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var custObj = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction');


var JobTest = function () {

    

    var args = arguments[0];
    var customObj = {};
    customObj.type = args.TestParam;
    customObj.key = args.testParam2;


    var objdelect = custObj.getCustomObject(customObj.type, customObj.key);


    if (objdelect != null) {
        transaction.wrap(function () {
            custObj.remove(objdelect);
        });

        return (new Status(Status.OK));
    } else {
        return (new Status(Status.ERROR));
    }
}
exports.JobTest = JobTest;