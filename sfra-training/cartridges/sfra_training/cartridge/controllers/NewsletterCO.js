'use strict';

var URLUtils = require('dw/web/URLUtils');
var server = require('server');
var stringUtils = require('dw/util/StringUtils');
var calendar = require('dw/util/Calendar');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction');


server.get(
    'Create',
    server.middleware.https,
    function (req, res, next) {
        var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
        // var jsonResponse = {}; 
        // jsonResponse.success = true;
        //jsonResponse.timeStamp = currentTimeStamp;
        //let r = res.json(jsonResponse);

        var fname = req.querystring.nome;
        var lname = req.querystring.nome;
        var email = req.querystring.email;


        if (email != null) {
            transaction.wrap(function () {
                var CustomObject = customObjectMgr.createCustomObject(dw.system.Site.getCurrent().getCustomPreferenceValue("newsletterobject")
                , currentTimeStamp);
                CustomObject.custom.nome = fname;
                CustomObject.custom.cognome = lname;
                CustomObject.custom.email = email;

            });

            res.json({
                success: true,
                'nome': fname,
                'cognome': lname,
                'email': email

            });
        } else {
            res.json({
                error: true
            });

        }
        next();
    });

server.get(
    'List',
    server.middleware.https,
    function (req, res, next) {
        var custObjlist = customObjectMgr.getAllCustomObjects(dw.system.Site.getCurrent().getCustomPreferenceValue("newsletterobject"));


        var list = [];

        while (custObjlist.hasNext()) {
            var listObj = custObjlist.next();
            var item = {};

            item.id = listObj.custom.timestamp;
            item.nome = listObj.custom.nome;
            item.cognome = listObj.custom.cognome;
            item.email = listObj.custom.email;
            list.push(item);

        }
        res.json(list);

        next();
    });

server.get(
    'Delete',
    server.middleware.https,
    function (req, res, next) {
        var id = req.querystring.id;
        var custObjDel = customObjectMgr.getCustomObject("newsletter1", id);

        transaction.wrap(function () {
            customObjectMgr.remove(custObjDel);
        });

        var deletion = {
            success: true
        };
        res.json(deletion);

        next();
    });

   


module.exports = server.exports();