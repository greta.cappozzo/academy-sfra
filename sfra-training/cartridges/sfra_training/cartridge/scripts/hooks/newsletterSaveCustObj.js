'use strict';


function saveCustObj(dati) {
    
    var customObjectMgr = require('dw/object/CustomObjectMgr');
    var transaction = require('dw/system/Transaction');
    var calendar = require('dw/util/Calendar');
    var stringUtils = require('dw/util/StringUtils');

    var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
    
    transaction.wrap(function () {

        var saveFname = dati.fname;
        var saveLname = dati.lname;
        var saveemail = dati.email;
        var test = 1;
        var CustomObject = customObjectMgr.createCustomObject('newsletterretry', currentTimeStamp);

        CustomObject.custom.fname = saveFname;
        CustomObject.custom.lname = saveLname;
        CustomObject.custom.email = saveemail;
        
    });
}

exports.saveCustObj = saveCustObj;