'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');


module.exports.render = function (context, modelIn) {
    var model = modelIn || new HashMap();
    var content = context.content;

    model.video = context.content.video;
    return new Template('experience/components/commerce_assets/component').render(model).text;
};