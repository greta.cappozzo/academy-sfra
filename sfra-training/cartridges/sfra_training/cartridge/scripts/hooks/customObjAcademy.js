'use sctrict';
var custObj = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction');

function objInfo (product) {

    var prodID = product.ID;
    var prodName = product.name;
    var prodDesc = product.shortDescription;

    transaction.wrap(function(){
        CustomObject = custObj.createCustomObject('AcademyProduct', prodID);
        CustomObject.custom.name = prodName;
        CustomObject.custom.shortDescription = prodDesc;
    });
    return {customObj : CustomObject};
}

exports.objInfo = objInfo;