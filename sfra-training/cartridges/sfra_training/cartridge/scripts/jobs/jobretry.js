'use strict';

var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var Logger = require('dw/system/Logger');
var customLogger = Logger.getLogger('JobRetry', 'response_jobretry');
var Transaction = require('dw/system/Transaction');


var JobRetry = function () {
    var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
    var args = arguments[0];

    var custObjlist = customObjectMgr.getAllCustomObjects('newsletterretry');
    // var notificationIterator = customObjectMgr.queryCustomObjects(storeHelpers.config.CUSTOM_OBJECT_TYPE_NAME, 'custom.isEmailSent != {0}', 'creationDate ASC', true);

    var reqObject = {};
    var error = false;

    var service = newsletterHelpers.getService('crm.newsletter.subscribe');

    while (custObjlist.hasNext()) {
        var listObj = custObjlist.next();
        reqObject = {
            properties: {
                firstname: listObj.custom.fname,
                lastname: listObj.custom.lname,
                email: listObj.custom.email
            }
        }

        var response = service.call(reqObject);
        // se il server risponde cancello il custom object
        if (response.ok) {
            var id = JSON.parse(response.object).id;
            var custObjDel = customObjectMgr.getCustomObject("newsletterretry", id);
            customLogger.warn("Customer has created successfully: " + listObj.custom.email);
            Transaction.wrap(function () {
                customObjectMgr.remove(custObjDel);
             });
         // se ci sono utenti già esistenti li cancello
         // sarebbe meglio con il numero del messaggio errore (controllando il significato del numero dell'errore)
        } else if (JSON.parse(response.errorMessage).message.indexOf('Contact already exists') > -1) {
            customLogger.warn("Customer duplicated: " + listObj.custom.email);
            Transaction.wrap(function () {
                customObjectMgr.remove(listObj);
            });
        } else {
            customLogger.warn("There was an error: " + response.msg);
            error = true;
        }
    }
    if (error) {
        return new Status(Status.ERROR);
    }
    return new Status(Status.OK);

}


exports.JobRetry = JobRetry;