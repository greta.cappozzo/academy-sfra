'use strict';

function dati(product){
    var productID = product.ID;
    var name = product.name;
    var shortDescription = product.shortDescription;

return {
    productID : productID,
    name : name,
    shortDescription :shortDescription,

}
}
exports.dati = dati;