'use strict';

var formValidation = require('base/components/formValidation');

module.exports = function () {
    $('form.newsletter-form').on('submit', function (e) {
        console.log('Prova');
        var $form = $(this);
        e.preventDefault();
        var url = $form.attr('data-url');
        $form.spinner().start();
        $('form.newsletter-form').trigger('newsletter:submit', e);
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: $form.serialize(),
            success: function (data) {
                console.log(data);
                $form.spinner().stop();
                if (!data.success) {
                    $('.subscription-feedback').text(data.response);
                    $('.newsletter-result').removeClass('hidden');
                    $('.subscription-feedback').addClass('failure');
                    // formValidation($form, data);

                } else {
                    // window.location.href = data.redirectUrl;
                    $('.subscription-feedback').text(data.response);
                    $('.newsletter-result').removeClass('hidden');
                    $('.subscription-feedback').addClass('success');
                    console.log(data);
                }
            },
            error: function (err) {
                if (err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                }
                $form.spinner().stop();
            }
        });
        return false;
    });



};