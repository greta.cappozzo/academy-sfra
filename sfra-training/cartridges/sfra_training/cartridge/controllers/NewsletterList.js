'use strict';

var server = require('server');
var URLUtils = require('dw/web/URLUtils');

server.get(
    'List',
    server.middleware.https,
    function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var service = newsletterHelpers.getServiceGetList('crm.newsletter.getlist');
        var response = service.call();
        var test = 1;


        if (response.ok) {

            //invece del for provo a fare il loop dentro a isml

            var objList = JSON.parse(response.object).results;
            var test = 0;      //variabile per debug

            res.render('/newsletter/newslettergetlist', {
                objList: objList
            });


            /* 
            ciclo for che ho sostituito:
            
            var jsonResList = [];
            var objList = JSON.parse(response.object).results; 
            var test = 0;
            for (var i = 0; i < obj.length; i++) {
                 var item = {};
                 item.firstname = obj[i].properties.firstname;
                 item.lastname = obj[i].properties.lastname;
                 item.email = obj[i].properties.email;

                 jsonResList.push(item);
             }
            res.render('/newsletter/newslettergetlist', {
             response: jsonResList
                
            }); */

        }
        next();
    }

);

module.exports = server.exports();