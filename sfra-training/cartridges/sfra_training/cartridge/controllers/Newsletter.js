'use strict';

//Use the following for CSRF protection: add middleware in routes and hidden field on form
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

var server = require('server');
var URLUtils = require('dw/web/URLUtils');

server.get(
    'Show',
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        // var actionUrl = dw.web.URLUtils.url('Newsletter-Handler');
        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();

        res.render('/newsletter/newslettersignup', {
            actionUrl: dw.web.URLUtils.url('Newsletter-Subscribe'),
            newsletterForm: newsletterForm
        });

        next();
    }
);

server.get(
    'Success',
    server.middleware.https,
    function (req, res, next) {
        res.render('/newsletter/newslettersuccess', {
            continueUrl: URLUtils.url('Newsletter-Show'),
            newsletterForm: server.forms.getForm('newsletter')
        });

        next();
    }
);

server.post(
    'Handler',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        var continueUrl = dw.web.URLUtils.url('Newsletter-Show');

        // Perform any server-side validation before this point, and invalidate form accordingly
        if (newsletterForm.valid) {
            // Send back a success status, and a redirect to another route
            res.json({
                success: true,
                redirectUrl: URLUtils.url('Newsletter-Success').toString()
            });
        } else {
            // Handle server-side validation errors here: this is just an example
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: URLUtils.url('Error-Start').toString()
            });
        }
        next();
    }

);

server.post(
    'Subscribe',
    server.middleware.https,
    function (req, res, next) {
        var customObjectMgr = require('dw/object/CustomObjectMgr');
        var transaction = require('dw/system/Transaction');
        var calendar = require('dw/util/Calendar');
        var stringUtils = require('dw/util/StringUtils');
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var newsletterForm = server.forms.getForm('newsletter');

        var email = newsletterForm.email.htmlValue;
        var firstname = newsletterForm.fname.htmlValue;
        var lastname = newsletterForm.lname.htmlValue;


        var service = newsletterHelpers.getService('crm.newsletter.subscribe');
        var reqObject = {
            "properties": {
                email: email,
                firstname: firstname,
                lastname: lastname
            }
        };

        var response = service.call(reqObject);

        /* var response = {
            ok : false,
          msg: "server unavailable"
        }; */

        var jsonResponse = {};

        if (response.ok) {

            jsonResponse = {
                email: email,
                firstname: firstname,
                lastname: lastname,
                id: JSON.parse(response.object).id,
                response: response.msg,
                success: true
                // redirectUrl: URLUtils.url('Newsletter-Success').toString()

            }

        } else {

            var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");

            var dati = {
                fname: firstname,
                lname: lastname,
                email: email
            };

            // chiamo hook che mi crea un custom object
            var newsCustObj = {}
            newsCustObj = dw.system.HookMgr.callHook('app.savehook', 'saveCustObj', dati);
            var test = 1;

            /* var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
                        
                        transaction.wrap(function () {
                        var CustomObject = customObjectMgr.createCustomObject('newsletterretry', currentTimeStamp);
               
                        CustomObject.custom.fname = firstname;
                        CustomObject.custom.lname = lastname;
                        CustomObject.custom.email = email;
                        var test = 1;
                         }), */

            jsonResponse = {
                response: 'There was an error:' + response.msg,
                success: false,
                extraVar: 'ciao'
            }
        };

        res.json(jsonResponse);

        next();
    });

    


module.exports = server.exports();